version="2.2.0"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/EliverLara/Nordic/releases/download/v$version/Nordic.tar.xz
fi

cd build
tar -xf $version.tar.gz
mkdir nordic-theme
mv Nordic nordic-theme
stow -t $HOME/.themes/ nordic-theme
