#!/bin/bash

mkdir build
cd build
git clone https://github.com/depsterr/spicetify-nord
cd ..
stow -t $HOME/.config/spicetify/Themes "build"
