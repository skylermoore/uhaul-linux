version="1.9.0"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/EliverLara/Nordic-Polar/releases/download/v$version/Nordic-Polar.tar.xz
fi

cd build
tar -xf $version.tar.gz
mkdir nordic-polar-theme
mv Nordic-Polar nordic-polar-theme
stow -t $HOME/.themes/ nordic-polar-theme
