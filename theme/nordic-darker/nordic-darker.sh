version="1.9.0"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/EliverLara/Nordic/releases/download/v$version/Nordic-darker.tar.xz
fi

cd build
tar -xf $version.tar.gz
mkdir nordic-darker
mv Nordic-darker nordic-darker
stow -t $HOME/.themes/ nordic-darker
