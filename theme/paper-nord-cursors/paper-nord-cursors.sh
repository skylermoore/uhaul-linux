#!/bin/bash

if [ ! -d build ]; then
	mkdir build
fi

curl -L https://gitlab.com/skylermoore/paper-nord-cursors/-/jobs/artifacts/master/download?job=render \
	-o build/paper-nord-cursors.zip
cd build
unzip paper-nord-cursors.zip -d paper-nord-cursors
cd paper-nord-cursors
./install.sh
