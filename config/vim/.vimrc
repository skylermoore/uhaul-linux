call plug#begin()
	"Theme
	Plug 'aditya-azad/candle-grey'
	"Tools
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	Plug 'terryma/vim-multiple-cursors'
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'
	Plug 'tpope/vim-commentary'
	Plug 'unblevable/quick-scope'
	"Languages
	Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
	Plug 'evanleck/vim-svelte'
	Plug 'cespare/vim-toml'
	Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
	Plug 'ziglang/zig.vim'
	Plug 'dart-lang/dart-vim-plugin'
call plug#end()

autocmd FileType svelte runtime! ftplugin/html/sparkup.vim

" We need to apply QuickScope colors before setting theme, or they won't
" show
augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=117 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=119 cterm=underline
augroup END

let g:accent_darken = 1
set background=dark
set t_Co=256
colorscheme candle-skyler

set relativenumber
set ignorecase
set incsearch
set colorcolumn=80,120
set smartindent
set smartcase
set noswapfile
set spelllang=en_us
set mouse=a
"set spell

" Output the current syntax group
nnoremap <C-a> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>

augroup vimrc
  au BufReadPre * setlocal foldmethod=indent
  au BufWinEnter * if &fdm == 'indent' | setlocal foldmethod=manual | endif
augroup END
set foldlevelstart=20

"set tabs to 4 columns, uses tabs since softtab is also 4, shifting code will
"also be 4
set tabstop=4 softtabstop=4 shiftwidth=4

"Character to show on lines that are wrapped
set showbreak=↪\ 
"set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨
set listchars=tab:\┊\ ,nbsp:␣,extends:⟩,precedes:⟨
set list

" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

"Always show status line
set laststatus=2
set statusline=
set statusline+=\ %F
set statusline+=%=
set statusline+=\ %y
set statusline+=\ %l:%L
set statusline+=\ %p%%

" set the undodir to undo-dir in .vim, creating it if needed.
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

"Map escaped characters to be alt. Needed for nor 8bit terminals
let c='j'
while c <= 'k'
	exec "set <A-".c.">=\e".c
	exec "imap \e".c." <A-".c.">"
	let c = nr2char(1+char2nr(c))
endw
set timeout timeoutlen=1 " Set timeout for escape code to 1ms.

"Move Lines with alt
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

"Move between split windows
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>

nnoremap <C-w> :vsplit<CR>
nnoremap <S-w> :split<CR>

"Open file search
nnoremap <C-p> :Files<CR>
nnoremap <C-e> :Explore<CR>

"Show and change buffer
nnoremap <C-b> :buffers<CR>:buffer<space> 

"COC goto commands
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

"Use Shift CR to enter normal mode
inoremap <S-CR> <Esc>

"Indent current block
nnoremap <C-i> [[=]]

"Comment visual selection
noremap <C-_> :Commentary<CR>

"Disable hinting from rust-analyzer
let g:coc_user_config = {
          \'suggest.noselect': v:true,
          \'rust-analyzer.inlayHints.typeHints': v:false,
          \'rust-analyzer.inlayHints.chainingHints': v:false,
          \'rust-analyzer.inlayHints.parameterHints': v:false
          \}
"Allow tab to select top COC suggestion
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" : "\<TAB>"
      "\ <SID>check_back_space() ? "\<TAB>" :
      "\ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
"And allow backspace to remove suggestion
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:svelte_preprocessors = ['typescript']

"Define multi_cursor keys to use Ctrl+d like in vscode 
let g:multi_cursor_use_default_mapping=0
let g:multi_cursor_start_word_key      = '<C-d>'
let g:multi_cursor_select_all_word_key = '<A-d>'
let g:multi_cursor_start_key           = 'g<C-d>'
let g:multi_cursor_select_all_key      = 'g<A-d>'
let g:multi_cursor_next_key            = '<C-d>'
let g:multi_cursor_prev_key            = '<C-p>'
let g:multi_cursor_skip_key            = '<C-x>'
let g:multi_cursor_quit_key            = '<Esc>'
