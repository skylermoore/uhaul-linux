# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Set prompts PS1
PS1="\w $ "

# Simple Alias and functions below
alias v=vim
alias open="xdg-open"
alias ls="ls --color"
alias l="exa"
alias ll="exa -l"
alias la="exa -la"
alias ..="cd .."

# Adds custom scripts to path if they have been installed
if [[ -e $HOME/.scripts/ ]]; then
	PATH=$HOME/.scripts:$PATH
fi

if [[ -e .bash_custom ]]; then
	source $HOME/.bash_custom
fi

if [[ -e /etc/profile.d/autojump.sh ]]; then
	source /etc/profile.d/autojump.sh
# Debian based system default install location
elif [[ -e /usr/share/autojump/autojump.sh ]]; then
	source /usr/share/autojump/autojump.sh
fi
