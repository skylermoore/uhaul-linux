version="1.2.9"

if [[ ! -e "build/v$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/v$version.tar.gz" \
		https://github.com/ducakar/openzone-cursors/archive/v$version.tar.gz
fi

cd build
tar -xf v$version.tar.gz
cd openzone-cursors-$version
make
mkdir openzone
mv OpenZone_* openzone
stow -t $HOME/.local/share/icons openzone
