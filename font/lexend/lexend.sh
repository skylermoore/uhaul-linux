if [[ ! -e "build/Lexend Deca.zip" ]]; then
	curl --create-dirs -Lo "build/Lexend Deca.zip" \
		"https://fonts.google.com/download?family=Lexend%20Deca"
fi

cd build
7za x "Lexend Deca.zip"
mkdir "Lexend Deca"
mv *.ttf "Lexend Deca"
stow -t "$HOME/.fonts" "Lexend Deca"
fc-cache -rv
