if [[ ! -e "build/Roboto.zip" ]]; then
	curl --create-dirs -Lo "build/Roboto.zip" \
		"https://fonts.google.com/download?family=Roboto"
fi

cd build
7za x "Roboto.zip"
mkdir "Roboto"
mv *.ttf "Roboto"
stow -t "$HOME/.fonts" "Roboto"
fc-cache -rv
