version="2020-0514"

if [[ ! -e "build/$version.zip" ]]; then
	curl --create-dirs -Lo "build/$version.zip" \
		https://www.brailleinstitute.org/wp-content/uploads/atkinson-hyperlegible-font/Atkinson-Hyperlegible-Font-Print-and-Web-$version.zip
fi

cd build
7za x $version.zip
cd "Atkinson-Hyperlegible-Font-Print-and-Web-$version"
stow -t "$HOME/.fonts" "Print Fonts"
fc-cache -rv
