version="2.4.456"

if [[ ! -e "build/pdfjs-$version-dist.zip" ]]; then
	curl --create-dirs -Lo "build/pdfjs-$version-dist.zip" \
		https://github.com/mozilla/pdf.js/releases/download/v$version/pdfjs-$version-dist.zip
fi

cd build
mkdir pdfjs
unzip pdfjs-$version-dist.zip -d pdfjs/pdfjs
stow -t ~/.local/share/qutebrowser pdfjs
