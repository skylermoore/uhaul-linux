version="1.15.2"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://golang.org/dl/go$version.linux-amd64.tar.gz
fi

cd build
sudo tar -C /usr/local -xzf $version.tar.gz
echo -e "\033[0;93mAdd the following to bashrc enable go \033[0;m"
echo
echo "export PATH=\$PATH:/usr/local/go/bin"
