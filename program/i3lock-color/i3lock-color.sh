version="2.12.c.4"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/Raymo111/i3lock-color/archive/$version.tar.gz
fi

cd build
tar -xf "$version.tar.gz" 
cd "i3lock-color-$version"
sh build.sh
cd build
su -c "make install"
