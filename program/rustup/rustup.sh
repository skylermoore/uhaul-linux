curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

echo -e "\033[0;93mAdd the following to bashrc enable rust \033[0;m"
echo
echo "source \$HOME/.cargo/env"
