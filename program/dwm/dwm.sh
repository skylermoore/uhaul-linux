version="dwm-6.2"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://dl.suckless.org/dwm/$version.tar.gz
fi

cd build
tar -xf $version.tar.gz
cd $version
patch -i ../../patches/dwm-pertag-6.2.diff
patch -i ../../patches/shiftview.diff
cp ../../config.h .
make
su -c "make install"
echo
echo -e "\033[0;93mInstall dwmbar script to have the status bar set when shown\033[0;m"
echo
