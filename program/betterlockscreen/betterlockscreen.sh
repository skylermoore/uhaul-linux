if [[ ! -e "build" ]]; then
	git clone https://github.com/pavanjadhaw/betterlockscreen build
fi
if [[ ! -e "$HOME/.scripts" ]]; then
	mkdir "$HOME/.scripts"
fi

mkdir build/install
mv build/betterlockscreen build/install/
stow -d build -t "$HOME/.scripts" install
