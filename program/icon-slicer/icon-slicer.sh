version="icon-slicer-0.3"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		http://freedesktop.org/software/icon-slicer/releases/$version.tar.gz
fi

cd build
tar -xf $version.tar.gz
cd $version
./configure
make
su -c "make install"
