version="st-0.8.3"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://dl.suckless.org/st/$version.tar.gz
fi

cd build
tar -xf $version.tar.gz
cd $version
patch -i ../../patches/st-scrollback-0.8.3.diff
cp ../../config.h . 
make
sudo make clean install
echo
echo -e "\033[0;93m To enable delete key: echo \"set enable-keypad on\" >> ~/.inputrc \033[0;m"
echo
