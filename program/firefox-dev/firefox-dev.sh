if [[ ! -e "build/firefox-dev-latest.tar.bz2" ]]; then
	curl --create-dirs -Lo "build/firefox-dev-latest.tar.bz2" \
		"https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US"
fi

cd build
echo "Extracting"
tar -xf firefox-dev-latest.tar.bz2
cd firefox
echo "Installing to: /usr/local/bin/firefox-dev"
sudo ln -s $PWD/firefox /usr/local/bin/firefox-dev
