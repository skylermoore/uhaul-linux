version="2.1.3"

if [[ ! -e "build/runelite.AppImage" ]]; then
	curl --create-dirs -Lo "build/runelite.AppImage" \
		"https://github.com/runelite/launcher/releases/download/$version/RuneLite.AppImage"
fi

cd build
chmod +x runelite.AppImage
echo "Installing to: /usr/local/bin/runelite.AppImage"
sudo ln -s $PWD/runelite.AppImage /usr/local/bin/runelite.AppImage
