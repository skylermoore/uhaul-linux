version="1.1"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/zayronxio/Zafiro-icons/archive/$version.tar.gz
fi

cd build
tar -xf $version.tar.gz
mkdir Zafiro-Icons
mv "Zafiro-icons-$version" Zafiro-Icons/
stow -t $HOME/.local/share/icons "Zafiro-Icons"
