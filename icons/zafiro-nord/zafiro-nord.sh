version="1.1"

if [[ ! -e "build/$version.tar.gz" ]]; then
	curl --create-dirs -Lo "build/$version.tar.gz" \
		https://github.com/zayronxio/Zafiro-icons/archive/$version.tar.gz
fi

cd build
tar -xf $version.tar.gz
mv "Zafiro-icons-$version/places/48" "Zafiro-icons-$version/places/48-blue"

cp -r "../patch/." "Zafiro-icons-$version/"
mkdir Zafiro-Icons-Nord
mv "Zafiro-icons-$version" "Zafiro-Icons-Nord/Zafiro-icons-$version-nord"
stow -t $HOME/.local/share/icons "Zafiro-Icons-Nord"
