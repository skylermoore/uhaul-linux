# Purpose

Scripts/files to make moving between linux environment easier.

Makes use of:

GNU Stow - manage symlink creation.

Curl - download files.

Some packages require build/runtime dependencies that these scripts do not take care of

## Cloning

### Shallow clone for simple configs etc.

git clone https://gitlab.com/skylermoore/uhaul-linux.git --depth 1

## Using install script

installing different elements on new systems:

```
./install program/st
./install theme/nordic
./install config/all
```
